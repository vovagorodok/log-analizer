#ifndef LOGLISTMODEL_H
#define LOGLISTMODEL_H

#include <QAbstractListModel>
#include "largefile.h"

class LogListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit LogListModel(const QString& fileName, QObject *parent = nullptr);

    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

protected:
    bool canFetchMore(const QModelIndex &parent) const override;

private:
    LargeFile largeFile;
};

#endif // LOGLISTMODEL_H
