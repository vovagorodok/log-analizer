#ifndef LARGEFILE_H
#define LARGEFILE_H
#include <QFile>
#include <QVector>

class LargeFile
{
public:
    LargeFile(const QString& fileName);
    virtual ~LargeFile();

    int linesCount() const;
    QString getLine(int lineNumber) const;

private:
    void readLinePositions();

    mutable QFile file;
    QVector<qint64> linePositions;
};

#endif // LARGEFILE_H
