#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <QScrollBar>
#include "loglistmodel.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::OpenFile);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::OpenFile()
{
    QString fileName = QFileDialog::getOpenFileName(this);
    qDebug() << fileName;
    //ui->listView->setUniformItemSizes(false);
    ui->listView->setSelectionMode(QAbstractItemView::SelectionMode::ExtendedSelection);
    ui->listView->setModel(new LogListModel(fileName));
}
