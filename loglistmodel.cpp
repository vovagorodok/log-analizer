#include "loglistmodel.h"
#include <QColor>
#include <QSize>

LogListModel::LogListModel(const QString &fileName, QObject *parent)
    : QAbstractListModel(parent), largeFile(fileName)
{
}

QVariant LogListModel::headerData(int, Qt::Orientation, int) const
{
    return QVariant();
}

int LogListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return largeFile.linesCount();
}

int LogListModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return 2;
}

QVariant LogListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= largeFile.linesCount() || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole) {
        return largeFile.getLine(index.row());
    }

    if (role == Qt::TextColorRole) {
        return index.row() % 2 ? QColor(Qt::green) : QColor(Qt::red);
    }

    if (role == Qt::SizeHintRole) {
        return QSize(10000, 20);
    }

    return QVariant();
}

bool LogListModel::canFetchMore(const QModelIndex&) const
{
    return false;
}
