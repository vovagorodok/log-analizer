#include "largefile.h"
#include <QDebug>

LargeFile::LargeFile(const QString &fileName) :
    file(fileName), linePositions()
{
    file.open(QIODevice::ReadOnly);
    readLinePositions();
}

LargeFile::~LargeFile()
{
    if (file.isOpen()) file.close();
}

int LargeFile::linesCount() const
{
    return linePositions.size();
}

QString LargeFile::getLine(int lineNumber) const
{
    file.seek(linePositions.at(lineNumber));
    return file.readLine();
}

void LargeFile::readLinePositions()
{
    if (file.isSequential())
    {
        qDebug() << "ERROR: file is sequential";
        return;
    }

    while (!file.atEnd())
    {
        file.readLine();
        linePositions.append(file.pos());
    }
}
